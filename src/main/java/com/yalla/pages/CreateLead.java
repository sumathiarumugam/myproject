package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class CreateLead extends Annotations{ 
	
	public CreateLead() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement eleSubmit;
	
	
	public CreateLead enterCompanyName(String data) {
		
		clearAndType(eleCompanyName, data);  
		return this; 
	}
	
	public CreateLead enterFirstName(String data) {
		clearAndType(eleFirstName, data); 
		return this; 
	}
	
	public CreateLead enterLastName(String data) {
		clearAndType(eleLastName, data); 
		return this; 
	}
	
	public ViewLead clickCreateLead1() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
          click(eleSubmit);  
          return new ViewLead();
         
	}
	
}







