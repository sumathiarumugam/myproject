package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class EditLead extends Annotations{ 
	
	public EditLead() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.ID,using="updateLeadForm_personalTitle") WebElement eleSalutation;
	@FindBy(how=How.ID,using="updateLeadForm_generalProfTitle") WebElement eleTitle;
	@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement eleSubmit;
	
	public EditLead enterSalutation(String data) {
		clearAndType(eleSalutation, data);  
		return this; 
	}
	
	public EditLead enterTitle(String data) {
		clearAndType(eleTitle, data); 
		return this; 
	}
	
	public ViewLead clickSubmit() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
          click(eleSubmit);  
          return new ViewLead();         
	}
	
}







