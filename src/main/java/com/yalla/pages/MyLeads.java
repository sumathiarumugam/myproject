package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.yalla.testng.api.base.Annotations;

public class MyLeads extends Annotations{ 

	public MyLeads() {
       PageFactory.initElements(driver, this);
	} 


	@FindBy(how=How.LINK_TEXT, using="Create Lead") WebElement eleCreateLead;
	@FindBy(how=How.XPATH, using="//div[text()='Lead ID']") WebElement eleLeadId;
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	
	public CreateLead clickCreateLead() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleCreateLead);  
		return new CreateLead();
	}
	
	public MyLeads sortLeadId() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLeadId);
		//wait = new WebDriverWait(driver, 10);
		return this;
	}
		
		
	public LoginPage clickLogout(){
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);  
		return new LoginPage();
	}
	}









