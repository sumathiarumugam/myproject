package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class ViewLead extends Annotations{ 

	public ViewLead() {
       PageFactory.initElements(driver, this);
	} 
	
	@FindBy(how=How.LINK_TEXT, using="Edit") WebElement eleEdit;
	@FindBy(how=How.LINK_TEXT, using="Delete") WebElement eleDelete;
	@FindBy(how=How.LINK_TEXT, using="Logout") WebElement eleLogout;
	public EditLead clickEditLead() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleEdit);  
		return new EditLead();
	}
	
	public MyLeads clickDelete() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleDelete);  
		return new MyLeads();
	}
	
	public void
	clickLogout() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);  
	//	return new LoginPage();
	}

}







