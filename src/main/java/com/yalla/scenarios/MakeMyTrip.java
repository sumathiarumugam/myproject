package com.yalla.scenarios;
					
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

/*This test case is to navigate to makemytrip website
 * search for flights from x to y
 * find a cheaper flight
 		
 */
public class MakeMyTrip {
		
	public static void main(String[] args) throws Exception {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://www.makemytrip.com/");
		Thread.sleep(2000);
		driver.findElementByXPath("//label[(text()='one way')]").click();
		driver.findElementByXPath("//label[(text()='one way')]//following::input[contains(@class,'input_fromto')][1]").clear();
		driver.findElementByXPath("//label[(text()='one way')]//following::input[contains(@class,'input_fromto')][1]").sendKeys("Cochin");
		Thread.sleep(2000);
		driver.findElementByXPath("//label[(text()='one way')]//following::input[contains(@class,'input_fromto')][2]").sendKeys("Singapore");
		Thread.sleep(1000);
		//check with sharath on the issue faced in selecting singapore
		driver.findElementByXPath("//span[contains(text(),'XSP')]").click();
		Thread.sleep(1000);
		driver.close();
		/*
		Actions actions = new Actions(driver);
		WebElement target = driver.findElementByLinkText("30");
		actions.click(target);
		/*driver.findElementByXPath("//div[@class='inputM depart_input inputHlp inputDateFilter']");
		Thread.sleep(1000);
		driver.findElementByXPath("//td[@class='  ui-datepicker-today']//following::a[1]");
		driver.findElementByXPath("//span[@class='o-i-passengers']");
		driver.findElementByXPath("//span[@class='o-i-passengers']//following::ul[@class='adult_counter']//li[text()='2']");
		driver.findElementByXPath("//span[@class='o-i-passengers']//following::ul[@class='child_counter']//li[text()='2']");
		driver.findElementByXPath("//span[@class='o-i-passengers']//following::a[@class='close_pax pull-right']");
		driver.findElementByXPath("//span[@class='o-i-passengers']//following::a[text()='Done']");
		driver.findElementById("searchBtn");*/
		
		
		
		
		
		
		
																										
	}

}
