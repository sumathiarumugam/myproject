package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003_EditLead";
		testcaseDec = "EditLeads in Leaftaps";
		author = "Gayatri";
		category = "smoke";
		excelFileName="TC003_EditLeads";
	} 

	@Test(dataProvider="fetchData") 
	public void editLead(String uName, String pwd,String companyName,String firstName,
						   String lastName,String Salutation,String Title ) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickcrmsfa()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(companyName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.clickCreateLead1()
		.clickEditLead()
		.enterSalutation(Salutation)
		.enterTitle(Title)
		.clickSubmit();
			
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	
}