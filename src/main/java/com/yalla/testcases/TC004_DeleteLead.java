package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC004_DeleteLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC004_DeleteLead";
		testcaseDec = "DeleteLeads in Leaftaps";
		author = "Gayatri";
		category = "smoke";
		excelFileName="TC002_CreateLead";
	} 

	@Test(dataProvider="fetchData") 
	public void DeleteLead(String uName, String pwd,String companyName,String firstName,String lastName) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickcrmsfa()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(companyName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.clickCreateLead1()
		.clickDelete()
		.sortLeadId()
		.clickLogout();
			
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	
}