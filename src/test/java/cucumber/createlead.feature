Feature: Create Lead in Leaftap Applications
Background:
Given open browser
And maximize browser
And enter url
Scenario Outline: Positive Create Flow
And enter username as <username>
And enter password as <password>
And click login button
And click the crmsfa link
And click the clickLeads tab
And click the createLead tab
And enter companyname as <companyName>
And enter firstname as <firstName>
And enter lastname as <lastName>
When click the createlead button
Then the test case is passed 	
Examples:
|username|password|companyName|firstName|lastName|
|DemoSalesManager|crmsfa|Facebook|Srisha|Vedavalli|
|DemoCSR|crmsfa||Google|Sundhar|Pichai|

