package steps;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	public ChromeDriver driver;
	@Given("open browser")
	public void openTheBrowser() {
	    System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe" );
	    driver = new ChromeDriver();
	}

	@And("maximize browser")
	public void maximizeTheBrowser() {
	    driver.manage().window().maximize();
	       
	}

	@And("enter url")
	public void enterTheUrl() {
	    driver.get("http://leaftaps.com/opentaps");
	    
	}

	@And("enter username as (.*)")
	public void enterTheUsername(String uname) {
	    driver.findElementById("username").sendKeys(uname);
	        
	}

	@And("enter password as (.*)")
	public void enterThePassword(String pword) {
	    driver.findElementById("password").sendKeys(pword);
	    
	}

	@And("click login button")
	public void clickTheLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}
	  


	@And("verify login is successful")
	public void verifyLoginIsSuccessful() {
	    // Write code here that turns the phrase above into concrete actions
	    
	}
	
	@And("click the crmsfa link")
	public void clickTheCrmsfaLink() {
	    // Write code here that turns the phrase above into concrete actions
	    driver.findElementByLinkText("CRM/SFA").click();;
	}
	
	@And("click the clickLeads tab")
	public void clickTheClickLeadsTab() {
	    // Write code here that turns the phrase above into concrete actions
	    driver.findElementByLinkText("Leads").click();
	}
	
	@And("click the createLead tab")
	public void clicktheCreateLeadsTab() {
	    // Write code here that turns the phrase above into concrete actions
	    driver.findElementByLinkText("Create Lead").click();
	}
	@And("enter companyname as (.*)")
	public void enterCompanyName(String cname) {
	    // Write code here that turns the phrase above into concrete actions
	    driver.findElementById("createLeadForm_companyName").sendKeys(cname);
	}
	@And("enter firstname as (.*)")
	public void enterFirstName(String fname) {
	    // Write code here that turns the phrase above into concrete actions
	    driver.findElementById("createLeadForm_firstName").sendKeys(fname);
	}
	
	@And("enter lastname as (.*)")
	public void enterLastName(String lname) {
	    // Write code here that turns the phrase above into concrete actions
	    driver.findElementById("createLeadForm_lastName").sendKeys(lname);
	}
	
	@When("click the createlead button")
	public void clickTheCreateLeadButton() {
	    // Write code here that turns the phrase above into concrete actions
	    driver.findElementByClassName("smallSubmit").click();
	}
	@Then("the test case is passed")
	public void theTestCaseIsPassed() {
	    // Write code here that turns the phrase above into concrete actions
	   
	}


}
