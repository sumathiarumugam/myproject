package steps;

import java.sql.Driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {




public ChromeDriver driver;
@Given("open the Browser")
public void openTheBrowser() {
    System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe" );
    driver = new ChromeDriver();
}

@And("maximise the browser")
public void maximiseTheBrowser() {
    driver.manage().window().maximize();
       
}

@And("enter the url")
public void enterTheUrl() {
    driver.get("http://leaftaps.com/opentaps");
    
}

@And("enter the username as (.*)")
public void enterTheUsername(String uname) {
    driver.findElementById("username").sendKeys(uname);
        
}

@And("enter the password as (.*)")
public void enterThePassword(String pword) {
    driver.findElementById("password").sendKeys(pword);
    
}

@When("click the login button")
public void clickTheLoginButton() {
	driver.findElementByClassName("decorativeSubmit").click();
}
  


@Then("verify login is successful")
public void verifyLoginIsSuccessful() {
    // Write code here that turns the phrase above into concrete actions
    
}



}
